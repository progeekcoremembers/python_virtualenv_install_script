#!/bin/bash

# Colors
CSI="\033["
CEND="${CSI}0m"
CRED="${CSI}1;31m"
CGREEN="${CSI}1;32m"

# Check root access
if [[ "$EUID" -ne 0 ]]; then
	echo -e "${CRED}Sorry, you need to run this as root${CEND}"
	exit 1
fi

# current APP_PATH
APP_PATH=$(cd "$(dirname "$0")"; pwd)
USER_PASS="somepassword"
USERS_GROUP="webapps"

# Clear log files
echo "" > /tmp/python3-virtualenv-install-output.log
echo "" > /tmp/python3-virtualenv-install-error.log

PYTHON3=$(which python3)
PYTHON3_VER=$(${PYTHON3} --version | cut -d " " -f 2)
PIP3_LOCAL_VERSION="---"
PIP3=$(which pip3)
if [[ -f ${PIP3} ]]; then
	PIP3_LOCAL_VERSION=$(${PIP3} --version | head -1 | tail -n 1 | cut -d " " -f 2)
fi

clear
echo ""
echo "Welcome to the PYTHON3 VirtualEnv Install"
echo ""
echo -ne "PIP 3 local version: ${CRED}${PIP3_LOCAL_VERSION}$CEND"
echo ""
echo -ne "Python 3 local version is: ${PYTHON3_VER}"
echo ""
echo ""
echo "Options for installing the package :"

while [[ $SHELL_TWEAK != "y" && $SHELL_TWEAK != "n" ]]; do
  read -p "       Install SHELL TWEAK [y/n]: " -e SHELL_TWEAK
done

while [[ $DEV_USER != "y" && $DEV_USER != "n" ]]; do
  read -p "       Create DEV user [y/n]: " -e DEV_USER
done

while [[ $PROD_USER != "y" && $PROD_USER != "n" ]]; do
  read -p "       Create PROD user [y/n]: " -e PROD_USER
done

#
# Build script
#

echo ""
read -n1 -r -p "Python3 VirtualEnv is ready to be installed, press any key to continue..."
echo ""

echo -ne "       Installing dependencies      [..]\r"
apt-get update 2>> /tmp/python3-virtualenv-install-error.log 1>> /tmp/python3-virtualenv-install-output.log
apt-get install -y python3-pip 2>> /tmp/python3-virtualenv-install-error.log 1>> /tmp/python3-virtualenv-install-output.log

if [ $? -eq 0 ]; then
  echo -ne "       Installing dependencies        [${CGREEN}OK${CEND}]\r"
  echo -ne "\n"
else
  echo -e "        Installing dependencies      [${CRED}FAIL${CEND}]"
  echo ""
  echo "Please look /tmp/python3-virtualenv-install-error.log"
  echo ""
  exit 1
fi

echo -ne "       Installing VirtualEnv packages [..]\r"
${PIP3} install --upgrade pip 2>> /tmp/python3-virtualenv-install-error.log 1>> /tmp/python3-virtualenv-install-output.log
${PIP3} install virtualenv virtualenvwrapper 2>> /tmp/python3-virtualenv-install-error.log 1>> /tmp/python3-virtualenv-install-output.log

if [ $? -eq 0 ]; then
  echo -ne "       Installing VirtualEnv packages [${CGREEN}OK${CEND}]\r"
  echo -ne "\n"
else
  echo -e "       Installing VirtualEnv packages [${CRED}FAIL${CEND}]"
  echo ""
  echo "Please look /tmp/python3-virtualenv-install-error.log"
  echo ""
  exit 1
fi

echo -ne "       Installing ${CGREEN}autoenv${CEND}             [..]\r"
if [ -d ${APP_PATH}/autoenv ]; then
  cd ${APP_PATH}/autoenv
  git reset --hard HEAD 2>> /tmp/python3-virtualenv-install-error.log 1>> /tmp/python3-virtualenv-install-output.log
  git clean -fd 2>> /tmp/python3-virtualenv-install-error.log 1>> /tmp/python3-virtualenv-install-output.log
  git pull origin master 2>> /tmp/python3-virtualenv-install-error.log 1>> /tmp/python3-virtualenv-install-output.log
  
  if [ $? -ne 0 ]; then
    rm -rf ${APP_PATH}/autoenv
	git clone git://github.com/kennethreitz/autoenv.git 2>> /tmp/python3-virtualenv-install-error.log 1>> /tmp/python3-virtualenv-install-output.log
  fi
else
  cd ${APP_PATH}
  git clone git://github.com/kennethreitz/autoenv.git 2>> /tmp/python3-virtualenv-install-error.log 1>> /tmp/python3-virtualenv-install-output.log
fi

if [ $? -eq 0 ]; then
  echo -ne "       Installing ${CGREEN}autoenv${CEND}             [${CGREEN}OK${CEND}]\r"
  echo -ne "\n"
else
  echo -e "       Installing ${CGREEN}autoenv${CEND}             [${CRED}FAIL${CEND}]"
  echo ""
  echo "Please look /tmp/python3-virtualenv-install-error.log"
  echo ""
  exit 1
fi

if [[ $SHELL_TWEAK = 'y' ]]; then
  echo -ne "       Installing ${CGREEN}Bash Tweaks${CEND}         [..]\r"
  if [ -d ${APP_PATH}/bash-tweaks ]; then
    cd ${APP_PATH}/bash-tweaks
	git reset --hard HEAD 2>> /tmp/python3-virtualenv-install-error.log 1>> /tmp/python3-virtualenv-install-output.log
	git clean -fd 2>> /tmp/python3-virtualenv-install-error.log 1>> /tmp/python3-virtualenv-install-output.log
	git pull origin master 2>> /tmp/python3-virtualenv-install-error.log 1>> /tmp/python3-virtualenv-install-output.log
	
	if [ $? -ne 0 ]; then
      rm -rf ${APP_PATH}/tweaks
	  git clone https://progeekro@bitbucket.org/progeekcoremembers/bash-tweaks.git 2>> /tmp/python3-virtualenv-install-error.log 1>> /tmp/python3-virtualenv-install-output.log
    fi
  else 	
    cd ${APP_PATH}
	git clone https://progeekro@bitbucket.org/progeekcoremembers/bash-tweaks.git 2>> /tmp/python3-virtualenv-install-error.log 1>> /tmp/python3-virtualenv-install-output.log
  fi
  
  if [ $? -eq 0 ]; then
    echo -ne "       Installing ${CGREEN}Bash Tweaks${CEND}         [${CGREEN}OK${CEND}]\r"
    echo -ne "\n"
  else
    echo -e "       Installing ${CGREEN}Bash Tweaks${CEND}         [${CRED}FAIL${CEND}]"
    echo ""
    echo "Please look /tmp/python3-virtualenv-install-error.log"
    echo ""
    exit 1
  fi
fi

echo -ne "       Setting up ${CGREEN}VirtualEnv${CEND}          [..]\r"
# remove any old python3 virtual end settings from .bashrc
sed -i '/## python3 virtualEnv/,/## python3 virtualEnv end/d' ~/.bashrc 2>> /tmp/python3-virtualenv-install-error.log 1>> /tmp/python3-virtualenv-install-output.log
#check if the python project folder is created, if not make it now
if [ ! -d /usr/local/pythonProject ]; then
  mkdir -p /usr/local/pythonProject
fi
# now that everything is removed let's append new settings
echo -e "## python3 virtualEnv
export VIRTUALENVWRAPPER_PYTHON=${PYTHON3}
export WORKON_HOME=/usr/local/pythonProject/.virtualenvs
export PROJECT_HOME=/usr/local/pythonProject
source $(which virtualenvwrapper.sh)
source ${APP_PATH}/autoenv/activate.sh
## python3 virtualEnv end" >> ~/.bashrc
source ~/.bashrc

if [ $? -eq 0 ]; then
  echo -ne "       Setting up ${CGREEN}VirtualEnv${CEND}          [${CGREEN}OK${CEND}]\r"
  echo -ne "\n"
else
  echo -e "       Setting up ${CGREEN}VirtualEnv${CEND}          [${CRED}FAIL${CEND}]"
  echo ""
  echo "Please look /tmp/python3-virtualenv-install-error.log"
  echo ""
  exit 1
fi
  
if [[ $SHELL_TWEAK = 'y' ]]; then
  # remove all reference on the shell tweaks
  sed -i '/## shell tweaks/,/## shell tweaks end/d' ~/.bashrc 2>> /tmp/python3-virtualenv-install-error.log 1>> /tmp/python3-virtualenv-install-output.log
  # now add all the shell tweaks from the repository
echo -e "\n## shell tweaks
. ${APP_PATH}/bash-tweaks/shell_tweeks/variables
. ${APP_PATH}/bash-tweaks/shell_tweeks/aliases
. ${APP_PATH}/bash-tweaks/shell_tweeks/functions
## shell tweaks end" >> ~/.bashrc
source ~/.bashrc
fi

if ([ $DEV_USER = 'y' ] || [ $PROD_USER = 'y' ]); then
  # for any user created we need to create a defalt group for both of the users
  if [ ! $(getent group webapps) ]; then
    groupadd ${USERS_GROUP} 2>> /tmp/python3-virtualenv-install-error.log 1>> /tmp/python3-virtualenv-install-output.log
  fi
  # permit the created users to be sudes
  sed -i '/includedir/s/^#//g' /etc/sudoers
fi

if [[ $DEV_USER = 'y' ]]; then
  echo -ne "       Creating Dev user ${CGREEN}pythonDev${CEND}    [..]\r"
  
  if [ ! $(getent passwd "pythonDev") ] ; then
    pass=$(echo ${USER_PASS} | openssl passwd -1 -stdin)
    adduser --system --ingroup ${USERS_GROUP} --shell /bin/bash pythonDev 2>> /tmp/python3-virtualenv-install-error.log 1>> /tmp/python3-virtualenv-install-output.log
	echo "pythonDev:${USER_PASS}" | chpasswd
	echo 'pythonDev ALL=(ALL:ALL) ALL' > /etc/sudoers.d/pythonDev
  fi
  
  if [ $? -eq 0 ]; then
    echo -ne "       Creating Dev user ${CGREEN}pythonDev${CEND}    [${CGREEN}OK${CEND}]\r"
    echo -ne "\n"
  else
    echo -e "       Creating Dev user ${CGREEN}pythonDev${CEND}    [${CRED}FAIL${CEND}]"
    echo ""
    echo "Please look /tmp/python3-virtualenv-install-error.log"
    echo ""
    exit 1
  fi
fi

if [[ $PROD_USER = 'y' ]]; then
  echo -ne "       Creating PROD user ${CGREEN}pythonProd${CEND}  [..]\r"
  
  if [ ! $(getent passwd "pythonProd") ] ; then
    pass=$(echo ${USER_PASS} | openssl passwd -1 -stdin)
	adduser --system --ingroup ${USERS_GROUP} --shell /bin/bash pythonProd 2>> /tmp/python3-virtualenv-install-error.log 1>> /tmp/python3-virtualenv-install-output.log
	echo "pythonProd:${USER_PASS}" | chpasswd
  fi
  
  if [ $? -eq 0 ]; then
    echo -ne "       Creating PROD user ${CGREEN}pythonProd${CEND}  [${CGREEN}OK${CEND}]\r"
    echo -ne "\n"
  else
    echo -e "       Creating PROD user ${CGREEN}pythonProd${CEND}  [${CRED}FAIL${CEND}]"
    echo ""
    echo "Please look /tmp/python3-virtualenv-install-error.log"
    echo ""
    exit 1
  fi
fi